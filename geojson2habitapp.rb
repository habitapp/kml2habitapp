#!/usr/bin/env ruby

# Solo permitimos un archivo a la vez
exit 1 if ARGV.size > 1

require 'json'
require_relative 'lib/habitapp'

geo = JSON.parse(ARGF.read)
habitapp = Habitapp.new(identifier: ENV['IDENTIFIER'],
                        password: ENV['PASSWORD'])

puts "Subiendo datos como: #{habitapp.identifier}"
puts "Para la categoría: #{habitapp.categoria['data']['attributes']['nombre']}"
puts ''

geo['features'].each do |feature|
  # normalizar los atributos
  properties = feature['properties'].map do |key, value|
    { key.downcase => value }
  end.inject(:merge)

  (lng, lat) = feature['geometry']['coordinates']
  coordenadas = { 'lng' => lng, 'lat' => lat }

  data = {
    'descripcion' => properties['nombre'],
    'direccion' => properties['direccion'] || properties['domicilio'],
    'informacion_de_contacto' => properties['telefono'],
    'coordenadas' => coordenadas
  }

  relevamiento = habitapp.relevamientos['data'].find do |r|
    r['attributes']['coordenadas'] == coordenadas
  end

  puts "* #{data['descripcion']}"

  if relevamiento
    puts "  Actualizando"
    data = relevamiento['attributes'].merge(data)
    data['extra'] = habitapp.extra(habitapp.relevamiento(relevamiento['id']))
    habitapp.update(relevamiento['id'], data)
  else
    puts "  Creando"
    data['extra'] = habitapp.extra
    # Guardar el punto si no existe
    habitapp.save(data)
  end
end
