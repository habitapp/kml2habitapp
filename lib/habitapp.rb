require 'securerandom'
require 'reverse_markdown'
require 'httparty'

# Acceso a la API de Habitapp
class Habitapp
  include HTTParty

  attr_reader :identifier, :relevamientos, :categoria

  # Público
  base_uri 'https://habitapp.org/api'
  # Privado
  #base_uri 'localhost:3000/api'

  # Crea una instancia de Habitapp, para comportarse como si fuéramos la
  # app (técnicamente, somos un cliente de la API de Habitapp)
  #
  # La contraseña e identificador son opcionales, si no los damos se
  # registra una instancia ad-hoc.
  def initialize(password: nil, identifier: nil)
    @password = password || SecureRandom.hex
    @identifier = identifier || register
    @token = login
  end

  # Trae todos los relevamientos de la categoría
  def relevamientos
    @relevamientos ||= self.class.get("/relevamientos.json?categoria=#{ENV['CATEGORIA']}")
  end

  # Trae un relevamiento por su id
  def relevamiento(id)
    self.class.get("/relevamientos/#{id}.json")
  end

  def categoria
    @categoria ||= self.class.get("/categorias/#{ENV['CATEGORIA']}.json")
  end

  # Devuelve la lista de campos extra vacíos para que se creen
  # correctamente en la base de datos (?)
  #
  # TODO: Si las plantillas corresponden con los datos del KML (por
  # alguna razón...) encontrar el valor.
  def extra(relevamiento = {})
    (categoria['included'] || []).map do |plantilla|
      next unless plantilla['type'] == 'plantilla'

      control = "control_#{plantilla['id']}"

      e = (relevamiento['included'] || []).find do |e|
        e['attributes']['name'] == control
      end

      if e
        { control => e['attributes']['valor'] }
      else
        { control => '' }
      end
    end.compact.inject(:merge)
  end

  # Registra una instancia
  def register
    self.class.post('/instancias.json', {
      body: {
        instancia: {
          password: @password
        }
      }
    })['data']['attributes']['identifier']
  end

  # Inicia sesión obteniendo un código de autorización (JWT)
  def login
    self.class.post('/instancias/login.json', {
      body: {
        instancia: {
          password: @password,
          identifier: @identifier
        }
      }
    })['access_token']
  end

  # Guardar un relevamiento en la base de datos.
  def save(relevamiento)
    self.class.post '/relevamientos.json', {
      body: {
        data: {
          type: 'relevamientos',
          attributes: con_categoria(relevamiento)
        }
      },
      headers: bearer
    }
  end

  # Actualizar un relevamiento en la base de datos.
  def update(id, relevamiento)
    self.class.put "/relevamientos/#{id}.json", {
      body: {
        data: {
          id: id,
          type: 'relevamiento',
          attributes: con_categoria(relevamiento)
        }
      },
      headers: bearer
    }
  end

  # Metadatos por defecto, enviamos el código de autorización
  def bearer
    { 'Authorization' => "Bearer #{@token}" }
  end

  def con_categoria(relevamiento)
    relevamiento['categoria_id'] = categoria['data']['id']

    relevamiento
  end
end
