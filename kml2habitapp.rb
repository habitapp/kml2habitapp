#!/usr/bin/env ruby

# Solo permitimos un archivo a la vez
exit 1 if ARGV.size > 1

require 'nokogiri'
require_relative 'lib/habitapp'

# El archivo puede ser entrada estándar o un archivo pasado como
# argumento:
#
# 1. ruby kml2habitapp.rb < doc.kml
# 2. ruby kml2habitapp.rb doc.kml
kml = Nokogiri::XML(ARGF.read)
# Para enviar los relevamientos con un identificador conocido, podemos
# pasarlos como variables de entorno:
#
# IDENTIFIER=hola PASSWORD=12345678 ruby kml2habitapp.rg doc.kml
habitapp = Habitapp.new(identifier: ENV['IDENTIFIER'],
                        password: ENV['PASSWORD'])

puts "Subiendo datos como: #{habitapp.identifier}"
puts "Para la categoría: #{habitapp.categoria['data']['attributes']['nombre']}"
puts ''

# Encontrar todos los puntos del KML
kml.css('Placemark').each do |placemark|
  # Obtener las coordenadas, descartando la altura
  (lng, lat, _) = placemark.css('coordinates').text.strip.split(',').map(&:to_f)

  coordenadas = { 'lng' => lng, 'lat' => lat }

  # Informar el punto que se está por subir
  puts "* #{placemark.css('name').text}"

  data = {
    'descripcion' => placemark.css('name').text,
    'informacion_de_contacto' => ReverseMarkdown.convert(placemark.css('description').text),
    'coordenadas' => coordenadas,
  }

  relevamiento = habitapp.relevamientos['data'].find do |r|
    r['attributes']['coordenadas'] == coordenadas
  end

  if relevamiento
    puts "  Actualizando"
    data = relevamiento['attributes'].merge(data)
    data['extra'] = habitapp.extra(habitapp.relevamiento(relevamiento['id']))
    habitapp.update(relevamiento['id'], data)
  else
    puts "  Creando"
    data['extra'] = habitapp.extra
    # Guardar el punto si no existe
    habitapp.save(data)
  end
end
