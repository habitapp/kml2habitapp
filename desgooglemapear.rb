#!/usr/bin/env ruby

require 'nokogiri'
require 'httparty'
require 'zip'
require 'pry'

ARGV.each do |url|
  if url.start_with? 'https://www\.google\.com/maps/d/embed?mid='
    puts "#{url} tiene que ser similar a https://www.google.com/maps/d/embed?mid=1yqIeHgatLWZsG1vpzhKSGMQ2-vc"
    next
  end

  kmz = HTTParty.get url.gsub('/embed?', '/kml?')
  tmp = Tempfile.new
  tmp.write kmz

  zip = Zip::File.open tmp
  kml = Nokogiri::XML zip.glob('doc.kml').first.get_input_stream.read
  fil = "./#{kml.css('name').first.text}.kml"

  if File.exist? fil
    puts "#{fil} ya fue descargado"
    next
  end

  puts fil
  doc = File.new(fil, File::RDWR | File::CREAT, 0o640)

  doc.write kml
end
