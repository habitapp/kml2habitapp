# KML2Habitapp

KML es el formato de datos que utilizan los mapas de Google Maps.  Con
este programa es posible tomar esos datos y cargarlos en Habitapp como
relevamientos asociados a una categoría.

# Instalación

Es necesario instalar ruby y bundler usando el gestor de paquetes de la
distribución GNU/Linux que usemos (posibles nombres `ruby` y
`ruby-bundler`).  Luego:

```bash
# Clonar el repositorio
git clone https://0xacab.org/habitapp/kml2habitapp.git
# Ingresar al directorio, hay que hacer esto cada vez que vamos a
# usar kml2habitapp
cd kml2habitapp
# Instalar librerías de ruby
bundle install
```

# Forma de uso

```
[IDENTIFIER=] [PASSWORD=] CATEGORIA= kml2habitapp [FILE]
```

IDENTIFIER es la variable de entorno opcional por la que pasamos un
identificador de Habitapp.  Si no pasamos ninguno, se genera un
identificar ad-hoc.

PASSWORD es la contraseña del identificador.  Es obligatoria si paramos
un identificador.

CATEGORIA es la categoría a la que se asocian los relevamientos.

FILE es el nombre del archivo, probablemente doc.kml.  El archivo se
puede pasar por entrada estándar también.

## Ejemplo

```bash
# Subir todos los puntos de un KML obtenido de Google Maps en la
# categoría 1
CATEGORIA=1 ./kml2habitapp doc.kml

# Extraer un KMZ (un KML comprimido) y enviarlo a Habitapp
unzip -p mapa.kmz doc.kml | CATEGORIA=1 ./kml2habitapp
```

## Obtener un KML de Google Maps

Si Google Maps no da la opción de descargarse el archivo KML, podemos
forzarlo con el programa asociado:

```bash
# La dirección tiene que ser la del mapa.  Si el mapa está incorporado
# en otra página, hay que encontrarlo en el código de esa página.
./desgooglemapear.rb "https://www.google.com/maps/d/embed?mid=1yqIeHgatLWZsG1vpzhKSGMQ2-vc"
```
